package com.learninng2.service;

import com.learninng2.model.CurrenciesModel;
import com.learninng2.repository.CurrenciesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CurrenciesService {

    @Autowired
    private CurrenciesRepository currenciesRepository;

    public List getDataCurrencies() {

        List currenciesList = new ArrayList<>();
        currenciesRepository.findAll().forEach(currenciesList::add);
        return currenciesList;
    }

    public CurrenciesModel createCurrencies(CurrenciesModel currenciesModel) {

        return currenciesRepository.save(currenciesModel);
    }

}



