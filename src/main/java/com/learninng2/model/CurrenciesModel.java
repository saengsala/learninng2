package com.learninng2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Entity
@NoArgsConstructor
@Table(name="currencies")
public class CurrenciesModel extends BaseModel  {

    @Id
    @NotNull(message = "createdBy can't null!")
    @Size(max = 3, message = "CurrencyCode should not be greater than 3")
    @Column(name = "code", columnDefinition = "varchar(3)", nullable = false)
    private String code;

    @Size(max = 255, message = "Description should not be greater than 255")
    @Column(name = "description", columnDefinition = "varchar(255)")
    private String description;

}


