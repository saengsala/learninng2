package com.learninng2.repository;

import com.learninng2.model.CurrenciesModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CurrenciesRepository extends JpaRepository<CurrenciesModel, String> {

}
