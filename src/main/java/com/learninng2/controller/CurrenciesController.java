package com.learninng2.controller;

import com.learninng2.model.CurrenciesModel;
import com.learninng2.service.CurrenciesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Controller
@Validated
public class CurrenciesController {

    private static final String DATA_NOT_FOUND = "Data not found";

    @Autowired
    private CurrenciesService currenciesService;

    @RequestMapping(value = "/api/currencies",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getData() {
        List value = currenciesService.getDataCurrencies();
        if (value == null){
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .contentType(MediaType.TEXT_PLAIN)
                    .body(DATA_NOT_FOUND);
        }

        return ResponseEntity.ok(value);
    }


    @RequestMapping(value = "/api/currencies",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@Valid @RequestBody CurrenciesModel currenciesModel) throws URISyntaxException {
        try {

            final CurrenciesModel returnModel = currenciesService.createCurrencies(currenciesModel);

            return ResponseEntity.created(new URI("/api/currencies/"+returnModel.getCode())).body(returnModel);
        } catch (ConstraintViolationException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(500)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body("This error may be due to network problems or database connection. Please try again or contact administrator.");
        }
    }


}
